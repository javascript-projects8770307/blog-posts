---
title: Building a CSV to JSON Converter with Express and Multer
date: 2024-08-13 22:11:00
tags: [Projects]
---

In the world of data manipulation, converting data from one format to another is a common and often necessary task. This blog will guide you through building a backend service that converts CSV files into JSON format using Node.js, Express, and Multer. We’ll cover each step in detail, including setup, implementation, testing, and common challenges.

## 1. Introduction

In many applications, especially those dealing with data integration or analysis, you may need to transform CSV files into JSON format. JSON is a popular format for data interchange, making it a suitable choice for APIs and data storage. This tutorial will help you create a simple backend service that accepts CSV file uploads, converts the contents into JSON, and returns the JSON data to the client.

## 2. Project Setup

To get started, we need to set up our Node.js environment and install the necessary packages.

### 2.1 Initializing the Project

Begin by creating a new directory for your project and initializing a Node.js project:

```bash
mkdir csv-to-json
cd csv-to-json
npm init -y
```

### 2.2 Installing Dependencies

We will use the following packages:

- **Express**: A web framework for Node.js.
- **Multer**: Middleware for handling file uploads.

Install these packages using npm:

```bash
npm install express multer
```

## 3. Implementing the Backend Service

We will now create the main backend service that handles file uploads and data conversion.

### 3.1 Creating index.js

Create an index.js file and add the following code:

```js
import express from "express";
import multer from "multer";
import fs from "fs";
import { csvParser } from "./controller/csv-parser-controller.mjs";

const app = express();
const port = 3000;

// Configure Multer for file uploads
const upload = multer({ dest: "uploads/" });
app.use(express.json());

// Home route
app.get("/", (req, res) => {
	res.status(200).json({ status: "200", data: [], message: "", error: "" });
});

// 404 route for undefined routes
app.get("*", (req, res) => {
	res.status(404).json({
		status: "404",
		data: [],
		message: "Route not found",
		error: "Page not found",
	});
});

// File upload route
app.post("/upload", upload.single("file"), (req, res) => {
	const file = req.file;
	if (!file) {
		return res.status(400).json({
			status: 400,
			data: [],
			message: "No file uploaded",
			error: "Bad request",
		});
	}

	fs.readFile(file.path, "utf-8", (err, data) => {
		if (err) {
			return res.status(500).json({
				status: "500",
				data: [],
				message: "Failed to read file",
				error: err.message,
			});
		}

		let result;
		try {
			result = csvParser(data);
		} catch (err) {
			return res.status(500).json({
				status: "500",
				data: [],
				message: "Failed to parse CSV",
				error: err.message,
			});
		}

		fs.unlink(file.path, (unlinkErr) => {
			if (unlinkErr) {
				console.error("Failed to delete file:", unlinkErr.message);
			}
		});

		res
			.status(200)
			.json({ status: "200", data: result, message: "", error: "" });
	});
});

app.listen(port, () => {
	console.log("Server listening on port", port);
});
```

### 3.2 Creating the CSV Parsing Controller

Create a controller directory and inside it, create csv-parser-controller.mjs with the following code:

```js
import fs from "fs";

function csvParser(data) {
	const lines = data?.trim().split("\n");
	const headers = parseCSVLine(lines[0]);
	let result = lines.slice(1).map((line) => {
		let obj = {};
		let values = parseCSVLine(line);
		headers.map((key, index) => {
			if (!obj[key]) {
				obj[key] = values[index];
			}
		});
		return obj;
	});
	return result;
}

function parseCSVLine(line) {
	const result = [];
	let current = "";
	let inQuotes = false;

	for (let i = 0; i < line.length; i++) {
		const char = line[i];

		if (char === '"' && inQuotes && line[i + 1] === '"') {
			current += '"';
			i++;
		} else if (char === '"') {
			inQuotes = !inQuotes;
		} else if (char === "," && !inQuotes) {
			result.push(current.trim());
			current = "";
		} else {
			current += char;
		}
	}
	result.push(current.trim());
	return result;
}

export { csvParser };
```

## 4. Testing the API

To ensure your API works correctly, use Postman or a similar tool to test the /upload endpoint.

### 4.1 Uploading a CSV File

- **Method**: POST
- **URL**: http://localhost:3000/upload
- **Body**: Use form-data to upload a CSV file.

### 4.2 Checking the Response

- **Success**: You should receive a JSON object representing the CSV data.
- **Errors**: The API will handle errors such as file read failures or parsing issues and return appropriate status codes and messages.

## 5. Challenges and Solutions

Here are some common challenges and solutions you might encounter:

### 5.1 Handling Large Files

For large files, consider using a streaming approach to process the file in chunks rather than loading it entirely into memory.

### 5.2 Different CSV Formats

CSV files can vary in format. Ensure your parser can handle various delimiters, quote characters, and escape sequences.

## 6. Conclusion

By following this guide, you’ve built a robust backend service capable of converting CSV files to JSON. This foundational project demonstrates the power of Node.js and Express for handling file uploads and data processing. You can extend this project by adding features like file validation, improved error handling, or integrating with a frontend application.
