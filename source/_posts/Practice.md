---
title: Practice
date: 2023-10-20 15:28:50
tags: [CLI]
categories: 
- [Practice Drills]
---

## Practice Drill 1

- Create the following directory structure. (Create empty files where necessary)
```
hello
├── five
│   └── six
│       ├── c.txt
│       └── seven
│           └── error.log
└── one
    ├── a.txt
```
    
- Rename the `one` directory to `uno`
- Move `a.txt` to the `two` directory

### Submission

Copy and paste the command for each section into a text file with explanation. At the end the exercise upload this file.

## File Permissions

Overview - [https://www.youtube.com/watch?v=D-VqgvBMV7g](https://www.youtube.com/watch?v=D-VqgvBMV7g)
Binary to Decimal Conversion for File Permissions - [https://www.youtube.com/watch?v=BmVmJi5dR9c](https://www.youtube.com/watch?v=BmVmJi5dR9c)

## Pipes and Redirection

Video 1 - [https://www.youtube.com/watch?v=Bzd7XfApxLI](https://www.youtube.com/watch?v=Bzd7XfApxLI)
Video 2 - [https://www.youtube.com/watch?v=VgbnndezHbw](https://www.youtube.com/watch?v=VgbnndezHbw)

```
├── b.txt
└── two
    ├── d.txt
    └── three
        ├── e.txt
        └── four
            └── access.log
```

- Delete all the files having the `.log` extension
- Add the following content to `a.txt`
```
Unix is a family of multitasking, multiuser computer operating systems that derive from the original AT&T Unix, development starting in the 1970s at the Bell Labs research center by Ken Thompson, Dennis Ritchie, and others
```
- Delete the directory named `five`

## Practice Drill 2

### Pipes

1. Download the contents of "Harry Potter and the Goblet of fire" using the command line from [here](https://raw.githubusercontent.com/bobdeng/owlreader/master/ERead/assets/books/Harry%20Potter%20and%20the%20Goblet%20of%20Fire.txt)
2. Print the first three lines in the book
3. Print the last 10 lines in the book
4. How many times do the following words occur in the book?
    * Harry
    * Ron
    * Hermione
    * Dumbledore
5. Print lines from 100 through 200 in the book
6. How many unique words are present in the book?

___________

### Processes, ports

1. List your browser's process ids (pid) and parent process ids(ppid)
2. Stop the browser application from the command line
3. List the top 3 processes by CPU usage.
4. List the top 3 processes by memory usage.
5. Start a Python HTTP server on port 8000
6. Open another tab. Stop the process you started in the previous step
7. Start a Python HTTP server on port 90
8. Display all active connections and the corresponding TCP / UDP ports.
9. Find the pid of the process that is listening on port 5432

____________

### Managing software

Use `apt` (Ubuntu) or `homebrew` (Mac) to:


1. Install `htop`, `vim` and `nginx`
2. Uninstall `nginx`
_____________

## Misc

1. What's your local IP address?
2. Find the IP address of `google.com`
3. How to check if Internet is working using CLI?
4. Where is the `node` command located? What about `code`?

### Submission

Copy and paste the command for each section into a text file with explanation. At the end the exercise upload this file.

## How Linux Works

Go through the link here: [https://neilkakkar.com/unix.html](https://neilkakkar.com/unix.html)

Philosophy:
- Processes
- Files

Files and the File System:
- iNodes
- File Permissions
- File Linking
- File Structure

Processes:
- Attributes
- Lifecycle
- File redirection

Layers in Unix:
- The Kernel
- Unix Utilities

How the shell works:
- The Pipe
- Everything about PATHs
- Writing Shell scripts

Package Managers

Brief History of Unix

## CLI Command (for Review):

### Bread and Butter Commands - all important:
* man - Super Important
* cd ( Understand flags - dot ., double dot .., tilda~, dash -)
* mkdir
* mv
* cp with recursive flag
* ls with different flags
* pwd
* rm
* chmod - Super important
* chown - Super important
* sudo
* apt
* touch
* cat
* less
* more
* tail
* rsync
* grep
* find - Super Important
* sort
* date
* tree (needs to be installed additionally)
* wc

### OS/Process Related Commands:
* ps
* top
* df
* uname
* free
* lspci
* kill (with flags)

### Network Related Commands:
* ping
* ifconfig
* ssh

### Bash Related Commands:
* xargs
* printenv
* nano
* awk
* sed
* Pipe operator |

## Actions that you should be able to perform:

* Create/Read/Update/Delete/Move files and folders from command line
* Check disk status
* Check status of processes, able to extract process ids ( hint: use pipe operator to combine ps, xargs and awk)
* Getting the most senior parent process
* Change file permissions. Able to explain and manipulate the numerical file permissions. (chmod and chown)
* Able to extract last x lines from files, get word count for a particular word, find a particular word. (basics of sed or awk would do)
* Basics of sed and awk.
* learn what is the difference between absolute and relative paths
* Practice using absolute and relative paths
* Learn how to use the find command
* Learn ls with the 5 most commonly used flags used such as:
 -- View hidden files
 -- Sort by time
 -- Reverse sort
 -- Human readable file sizes
 -- Combining flags to get hidden files, sorted by time in reverse with human readable file sizes.
* Find out what are terminal control codes such as Ctrl + D, Ctrl + C, Ctrl + Z etc and use them
* Find out the difference between Ctrl + C and Ctrl + Z
* Find out how to use Ctrl + R to reverse search
* Find out how to use tab autocompletion
* Find out how to use the arrow keys to navigate history

## Sample Review Questions:

1. Go into your home directory
2. Create a directory d1
3. Create a file a.txt inside it
4. Check permission of a.txt. What are the permissions in decimal format?
5. What are three elements in the permission? Do you understand conversion of decimal to binary?
6. Change the permissions of a.txt to 755?
7. Add a directory d2
8. Add b.txt inside d2
9. Change permissions of d2 (and everything inside) to 755
10. Start the Firefox browser.
11. List all processes in your computer
12. Find pid of Firefox Browser. Difference between parent process and child process. (hint: you need to learn pipes)
13. Kill the process (hint: pipes, awk, xargs, kill)
14. What is your user in Linux?
15. What is your group in Linux?
16. What is your computer architecture? (hint: uname command, learn the flags)
17. What is your audio driver? (hint: lspci, learn pipes and grep)
18. Go to home folder. Use find command to find all occurrences of "java" text anywhere in any filename or directory name in your system?
19. List everything in the home directory to get all files (including hidden), sorted by time in reverse with human readable file sizes.
20. Get last x lines for Harry Potter. Get word counts for particular words.

### Questions:
1. What is the difference between service and application?
2. What are these wildcards ~, ., .., * and ??
3. What are the different flags for kill? Why do we use kill -9 in general?
4. Are you clear about file permissions? Explain them? chmod and chown commands?
5. Usage of Ctrl+R to search previously run commands, arrow keys, tab autocompletion.
