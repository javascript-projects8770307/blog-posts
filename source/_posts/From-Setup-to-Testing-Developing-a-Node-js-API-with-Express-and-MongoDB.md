---
title: "From Setup to Testing: Developing a Node.js API with Express and MongoDB"
date: 2024-08-11 10:20:41
tags: [Projects]
---

### Building a RESTful API with Express, MongoDB, and Mongoose: A Step-by-Step Guide

## Introduction

In today's web development landscape, creating a robust and scalable backend is crucial for building full-stack applications. Whether you're working on a personal project or a professional application, understanding how to design and implement a RESTful API is a valuable skill.

In this blog, we'll walk through the process of building the backend for a simple Student Management System using **Node.js**, **Express**, **MongoDB**, and **Mongoose**. We'll start by setting up our project structure, define our data models, and create API endpoints to manage student records. Finally, we'll test our API using **Postman** to ensure everything is functioning as expected.

By the end of this blog, you'll have a solid understanding of how to connect a Node.js server to a MongoDB database, handle HTTP requests, and perform CRUD operations. Let's dive in and get started with building the backend!

## Step 1: Setting Up the Backend Project

Before diving into code, the first step is to set up the structure of your backend project. This involves creating the necessary directories and files, installing dependencies, and initializing a Node.js project.

### 1.1 Initialize the Project

Start by creating a new directory for your project and navigating into it:

```bash
mkdir student-management-backend
cd student-management-backend
```

Next, initialize a new Node.js project. This will create a package.json file where you can manage your project’s dependencies:

```bash
npm init -y
```

The -y flag automatically answers "yes" to all prompts, creating the package.json with default settings.

### 1.2 Install Dependencies

For this project, you'll need several packages. Use the following command to install them:

```bash
npm install express mongoose cors
npm install --save-dev nodemon
```

- **Express**: A minimal and flexible Node.js web application framework that provides a robust set of features for building web and mobile applications.
- **Mongoose**: An Object Data Modeling (ODM) library for MongoDB and Node.js, providing a straightforward way to model your data.
- **Cors**: A middleware that allows you to enable Cross-Origin Resource Sharing, which is essential when your frontend and backend are hosted on different domains.

Additionally, you can install nodemon as a development dependency to automatically restart your server when files change:

### 1.3 Set Up the Project Structure

Create the necessary directories and files for your project:

```bash
mkdir persistence
mkdir persistence/models
mkdir controller
mkdir routes
touch app.mjs
touch persistence/index.mjs
touch persistence/models/student.mjs
touch controller/studentController.mjs
touch routes/studentRoutes.mjs
touch routes/teacherRoutes.mjs
```

Your project structure show now look like this:

```
student-management-backend/
├── app.mjs
├── package-lock.json
├── package.json
├── controller
│   └── studentController.mjs
├── persistence
│   ├── index.mjs
│   └── models
│       └── student.mjs
└── routes
    ├── studentRoutes.mjs

```

### 1.4 Configure package.json Scripts

To make running your server easier during development, update the scripts section of your package.json to include a start script:

```bash
    "scripts": {
    "start": "node app.js",
    "dev": "nodemon app.js"
    }
```

- start: Runs the server using Node.js.
- dev: Runs the server using Nodemon, which will restart the server whenever a file change is detected.

Now, with the controller directory in place and using ES modules (.mjs files), you’re all set up and ready to start building the core of your backend application. In the next step, we’ll connect to MongoDB and set up the Mongoose models.

### Key Points in Step 1:

- **Initialization**: Guides the reader through initializing the Node.js project and installing dependencies.
- **Structure**: Helps set up the necessary file and directory structure.
- **Package Configuration**: Adds useful scripts to `package.json` for easy development and testing.

This step lays a strong foundation for the rest of your project.

## Step 2: Connecting to MongoDB with Mongoose

In this step, we'll focus on setting up the connection between your Node.js application and MongoDB using Mongoose. This is a crucial part of the backend setup, as it establishes how your application will interact with the database.

### 2.1: Setting Up Mongoose Connection

To start, we need to connect our application to MongoDB using Mongoose. Open the persistence/index.mjs file and add the following code:
import mongoose from "mongoose";

```js
export const connect = (url) => {
	return mongoose.connect(url, {
		useNewUrlParser: true,
		useUnifiedTopology: true,
	});
};
```

- **Explanation**: Here, we define a connect function that takes a url parameter, representing the MongoDB connection string. The useNewUrlParser and useUnifiedTopology options ensure compatibility with the latest MongoDB server features.

### 2.2: Connecting to MongoDB in app.mjs

Now, let's establish this connection in our main application file, app.mjs:

```js
import { connect } from "./persistence/index.mjs";
import express from "express";
import cors from "cors";

const app = express();
const port = 5000;
const url = "mongodb://localhost:27017/school";

// Middleware
app.use(cors());
app.use(express.json());

// MongoDB Connection
connect(url)
	.then(() => {
		console.log("Connected to MongoDB successfully");
	})
	.catch((err) => {
		console.error("Failed to connect to MongoDB", err);
	});

app.listen(port, () => {
	console.log(`Server is running on http://localhost:${port}`);
});
```

- **Explanation**: In this snippet, we import the connect function from index.mjs and use it to establish a connection to MongoDB when the server starts. If the connection is successful, a confirmation message is logged. If it fails, an error message is displayed.

## Step 3: Defining the Student Model with Mongoose

### 3.1: Creating the Student Schema

Navigate to persistence/models/student.mjs and define the schema:

```js
import mongoose from "mongoose";

const studentSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: true,
	},
	lastName: {
		type: String,
		required: true,
	},
	class: {
		type: String,
		required: true,
	},
	rollNumber: {
		type: Number,
		required: true,
		unique: true,
	},
});

export const Student = mongoose.model("Student", studentSchema);
```

- **Explanation**: The studentSchema defines the structure of the student documents in the MongoDB collection. Fields like firstName, lastName, class, and rollNumber are specified, with validation rules to ensure data integrity.

## Step 4: Creating the Controller for Student Operations

To keep your application modular and organized, we’ll create a controller to handle student-related operations.

### 4.1: Implementing the Student Controller

In controller/studentController.mjs, implement the following function to handle student operations:

```js
iimport { Student } from "../persistence/models/student.mjs";

// Function to insert a new student
export async function insertStudent(req, res) {
  const data = req.body;
  try {
    const newStudent = new Student(data);
    const savedStudent = await newStudent.save();
    res.status(200).json({
      status: 200,
      message: "Student saved successfully",
      data: savedStudent,
    });
  } catch (err) {
    res.status(400).json({ error: "Failed to save student" });
  }
}

// Function to get all students
export async function getAllStudents(req, res) {
  try {
    const students = await Student.find();
    res.status(200).json({
      status: 200,
      message: "Students fetched successfully",
      data: students,
    });
  } catch (err) {
    console.error("Error fetching students", err);
    res.status(400).json({ error: "Failed to load students" });
  }
}
```

- **Explanation**: The insertStudent function receives student data, creates a new instance of the Student model, and saves it to the database. It returns a success or error message based on the outcome.

#### Let's proceed to Step 5, where we’ll define routes to interact with the Student model, and then test the API using Postman.

## Step 5: Creating Routes for Student Operations

In this step, we’ll create routes that allow us to handle CRUD (Create, Read, Update, Delete) operations for student data. These routes will be defined in a separate file to keep the code organized.

### 5.1: Setting Up Student Routes

Create routes/studentRoutes.mjs to handle student-related requests:

```js
import express from "express";
import {
	insertStudent,
	getAllStudents,
} from "../controller/studentController.mjs";

const router = express.Router();

router.post("/", insertStudent);
router.get("/", getAllStudents);

export default router;
```

### 5.2: Integrating Routes with the Main Application

Ensure your routes are properly integrated into app.mjs.

```js
import { connect } from "./persistence/index.mjs";
import studentRoutes from "./routes/studentRoutes.mjs";
import express from "express";
import cors from "cors";

const app = express();
const port = 5000;
const url = "mongodb://localhost:27017/school";

// Middleware
app.use(cors());
app.use(express.json());

// MongoDB Connection
connect(url)
	.then(() => {
		console.log("Connected to MongoDB successfully");
	})
	.catch((err) => {
		console.error("Failed to connect to MongoDB", err);
	});

// Use student routes
app.use("/api/students", studentRoutes);

// Default route
app.get("/", (req, res) => {
	res.status(200).json({ status: 200, message: "ok" });
});

// 404 handler
app.use("*", (req, res) => {
	return res.status(404).json({ message: "Route not found", status: 404 });
});

app.listen(port, () => {
	console.log(`Server is running on http://localhost:${port}`);
});
```

## Step 6: Testing the API with Postman

Testing is crucial to ensure your API works as expected.

### 6.1: Testing the POST /api/students Endpoint

1. Open Postman and create a new POST request.
2. Set the URL to http://localhost:5000/api/students.
3. In the Body tab, select raw and choose JSON.
4. Enter the following JSON data:

```js
{
  "firstName": "John",
  "lastName": "Doe",
  "class": "10th",
  "rollNumber": 123
}
```

5. Click Send and verify the success response.

### 6.2: Testing the GET /api/students Endpoint

Create a new GET request.
Set the URL to http://localhost:5000/api/students.
Click Send and check the response containing the list of students.

## Conclusion

Congratulations! You’ve built a full-featured backend application using Node.js, Express, MongoDB, and Mongoose. This guide has walked you through setting up the project, defining models, creating routes and controllers, and testing the API.

Feel free to extend this application with additional features or integrate it with a frontend to create a complete full-stack solution. Keep experimenting and happy coding!
