---
title: Promises
date: 2023-03-14 14:01:38
tags: [JavaScript]
---

# Promises

A promise is an object which is used to store the outcome of asynchronous functions or operations, whether they are successful or not.
Any asynchronous function which takes some time to produce the result is passed as a producing code and the promise makes the result available to the codes when it is done.
Promises are the simplest way to get rid of callback hells where one asynchronous function is dependent on another one. It can handle asynchronous operations very easily and also provide better readability, easy-to-understand and easy-to-debug code.

A Promise can be created using a constructor as seen below:

        const promise = new Promise((resolve, reject)=>{
            // Code to do some operation...

        })

In the above example, we can see that the Promise constructor is taking a callback function as an argument which is taking two arguments 'resolve' and 'reject'.
These resolve and reject will get called if the operation of the code succeeded or failed.
The 'resolve' method will get called if the code succeeded and the 'reject' will get called if the asynchronous operation fails for any reason.


These Promises can be used by using .then and .catch methods.
* then() will get called if the operation succeeded it can also take data from the 'promise' to execute it in further operations.
* catch() will get called if the operation fails or gets rejected for some reason.


        const promise = new Promise((resolve, reject) => {
            let number = 10;
            if(number < 0){
                reject("The Number is negative.");
            } else {
                resolve("The Number is positive.");
            }
        })

        promise.then((message)=>{
            console.log(message);
        }).catch((message)=>{
            console.log(message);
        })

Output

        The number is positive.

In the above code, we have created a promise by using the promise constructor and passing a callback function as an argument which takes two arguments to resolve and reject.
We are checking if the number is positive or not. if it is negative then the reject method will get called and the promise will return reject which can be used using the .catch() method while consuming the code result.
And if the number is positive then the resolve method will get called which can be consumed by using .then() method.


## Promise states

A Promise has three states and it will remain in one of these states.
1. pending : It means that the promise is in the initial state and the promise is not fulfilled nor rejected.
2. fulfilled : It means that the operation is completed successfully.
3. rejected : It means that the operation is not completed or failed for some reason.

The promise is called settled when the state of promise is fulfilled or rejected.


## Promise.all()

Promise.all() is the method of JavaScript which is used to handle asynchronous operations. It takes an array of promises which is iterable as input and returns a single Promise resolve if all of the promises resolved its operation(Even passed an empty iterable).
If any one of the promises within the array rejects its promise then Promise.all() will asynchronously reject its promise. It does not matter that other promises have resolved their promises.

For example:

        const promise1 = new Promise((resolve, reject)=>{
            resolve('Promise 1 completed');
        })
        const promise2 = new Promise((resolve, reject)=>{
            resolve('Promise 2 completed');
        })
        const promise3 = new Promise((resolve, reject)=>{
            resolve('Promise 3 completed');
        })


        Promise.all([promise1, promise2, promise3]).then(messages =>{
            console.log(messages);
        })

Output

    [ 'Promise 1 completed', 'Promise 2 completed', 'Promise 3 completed' ]

In the above code, we have created three promises and all resolve their operations.
In Promise.all() method we are passing an array of all promises which is also iterable.
Here all promises are resolved so the output we are getting is in the array.

If anyone the Promises is rejected then Promise.all will reject its promise:

        const promise1 = new Promise((resolve, reject)=>{
            resolve('Promise 1 completed');
        })
        const promise2 = new Promise((resolve, reject)=>{
            reject('Promise 2 rejected');
        })
        const promise3 = new Promise((resolve, reject)=>{
            resolve('Promise 3 completed');
        })

        Promise.all([promise1, promise2, promise3]).then(messages => {
            console.log(messages);
        }).catch(message =>{
            console.log(message);
        })

Output

        Promise 2 rejected

In the above code, there are three promises but the promise2 has not fulfilled its promise it rejects its promise while all other promises have resolved their promises.
Even only one of the promises is rejected the Promise.all() methods will reject its promise. As we can see in the output.

If any one of the promises takes a longer time to execute. Then the Promise.all() method will wait for all fulfilment or the first rejection.


        const promise1 = Promise.resolve('Promise 1 resolved');
        const promise2 = 'Hello';
        const promise3 = new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve("Promise 3 is resolved");
        }, 2000);
        });

        Promise.all([promise1, promise2, promise3]).then((values) => {
        console.log(values); 
        });

Output

         [ 'Promise 1 resolved', 'Hello', 'Promise 3 is resolved' ]

In the above code Even if promise 3 has taken 2 seconds to complete the Promise.all() method waits for the fulfilment of all promises.


## Promise.allSettled()

Promise.allSettled() is a method in JavaScript, which takes an array(iterable) of promises and checks if promises are settled whether it is resolved or rejected and returns a single Promise.

For example: 

        const promise1 = new Promise((resolve, reject)=>{
            resolve("Promise 1 resolved");
        })
        const promise2 = 'Hello';
        const promise3 = new Promise((resolve, reject) =>{
            resolve("Promise 3 rejected");
        })
        const promise4 = new Promise((resolve, reject)=>{
            reject("Promise 4 rejected");
        })

        Promise.allSettled([promise1, promise2, promise3, promise4]).then(messages =>{
            console.log(messages);
        })
    
output:

    [
        { status: 'fulfilled', value: 'Promise 1 resolved' },
        { status: 'fulfilled', value: 'Hello' },
        { status: 'fulfilled', value: 'Promise 3 rejected' },
        { status: 'rejected', reason: 'Promise 4 rejected' }
    ]

In the above code, we have created 4 promises and put them together in an array, and passed the array as an argument to Promise.allSettled() method.
Promise.allSetelled() method shows all of the promises status whether it is fulfilled or rejected. if the promise is rejected then it shows the reason behind rejected, and if it is fulfilled then it shows the value of the promise.

If any empty iterable is passed in the Promise.allSettled() method then the return value will be already fulfilled.

For example:

        Promise.allSettled([]).then(messages =>{
            console.log(messages);
        })

Output

        []

# Promise.any()

Promise.any() method is a JavaScript method which takes an array(iterable) of promises as an argument. If any one of the promises in the array is fulfilled then the Promise.any() method will return a single promise resolved with the result of the first fulfilled Promise.

This method ignores the rejected promises and only cares about the first fulfilled promises.

Let's take an example:

        const firstPromise = new Promise((resolve, reject)=>{
            setTimeout(()=>{
                resolve("First Promise Resolved");
            },2 * 1000)
        })

        const secondPromise = new Promise((resolve, reject)=>{
            resolve("Second Promise Resolved");
        })

        const thirdPromise = new Promise((resolve, reject)=>{
            reject("Third Promise Rejected");
        })


        Promise.any([firstPromise, secondPromise, thirdPromise]).then(message=>{
            console.log(message);
        })

Output:

    Second Promise Resolved

In the above code, we can see that the first promise is resolved but it is taking 2 seconds to resolve its promise.
Where the second promise is resolved in less amount of time. And the third Promise is rejected already.
The Promise.any() method does not wait for the promises to be completed. It will return the first promise which is fulfilled.

In case if all promises are rejected then Promise.any() method will return an "AggregateError: All Promises were rejected" Error.


        const firstPromise = new Promise((resolve, reject)=>{
            reject('First Promise rejected')
        })

        const secondPromise = new Promise((resolve, reject)=>{
            reject("Second Promise rejected");
        })

        const thirdPromise = new Promise((resolve, reject)=>{
            reject("Third Promise Rejected");
        })


        Promise.any([firstPromise, secondPromise, thirdPromise]).then(message=>{
            console.log(message);
        })

Output:

        [AggregateError: All promises were rejected] {
            [errors]: [
                'First Promise rejected',
                'Second Promise rejected',
                'Third Promise Rejected'
                ]
        }

If any empty iterable is passed Then the promise will return Already rejected.
For example:

       Promise.any([]).then(messages =>{
            console.log(messages);
        })

        //Output:
        [AggregateError: All promises were rejected] { [errors]: [] }


## Promise.race()

Promise.race() is a JavaScript object which takes an array(iterable) and checks which promise is settled first after checking it returns a single Promise with the state of the first settled promise.
It doesn't care about the state of the promises either it fails or succeeds. 



        const promise1 = new Promise((resolve, reject)=>{
            setTimeout(()=>{
                resolve("Promise 1 resolved");
            },2000)
        })
        const promise2 = new Promise((resolve, reject) =>{
            setTimeout(()=>{
                resolve("Promise 3 resolved");
            },3000)
        })
        const promise3 = new Promise((resolve, reject)=>{
            setTimeout(()=>{
                resolve("Promise 4 resolved");
            },2500)
        })
        const promise4 = new Promise((resolve, reject)=>{
            setTimeout(()=>{
                reject("Promise 4 rejected");
            },500)
        })

        Promise.race([promise1, promise2, promise3,promise4]).then((messages) =>{
            console.log(messages);
        }).catch(message =>{
            console.log(message);
        });

Output

        Promise 4 rejected

In the above code promise 4 is settled first because it takes 500 ms which is lower than the rest of the promises. 
So the Promise.race method is returning the single Promise of the first settled promise.

If an empty iterable is passed into Promise.race method then the state of the promise will be pending forever.

        const promise = Promise.race([]);

        console.log(promise);

        //Output : Promise { <pending> }