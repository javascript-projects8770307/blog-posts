---
title: JavaScript-Basics
date: 2023-11-17 16:12:08
tags: [FullStack Path]
categories:
  - [Introduction to JavaScript]
---

## 1. Introduction

[CodeAcademy Tutorial](https://www.codecademy.com/learn/introduction-to-javascript)

You can also watch a popular JavaScript basics crash course video on Youtube.

## 2. Modules

[Read this article](https://www.freecodecamp.org/news/node-module-exports-explained-with-javascript-export-function-examples/)

Video - [https://www.youtube.com/watch?v=hyYbs3SANRo](https://www.youtube.com/watch?v=hyYbs3SANRo)

## 3. Best Practices: Indentation

Ensure that your code indentation is good. Submitting code that looks like the example given is unprofessional and makes it difficult to read for you as well as anyone who has to work with your code.

Bad indentation:

```javascript
function problem1(inventory, passid) {
  if (Array.isArray(inventory)) {
    return inventory;
  } else {
    return [];
  }
}

module.exports = problem1;
```

Good indentation:

```javascript
function problem1(inventory, passid) {
  if (Array.isArray(inventory) && passid < 50) {
    return inventory;
  } else {
    return [];
  }
}

module.exports = problem1;
```

An easy way to indent your code is to press Ctrl + Shift + I in VSCode.

If you are on a Mac, try Cmd + Shift + I

## 4. Best Practices: Variable Naming

Ensure that your variable names are descriptive. Variables names looks like the examples below are unprofessional and makes it difficult to read for you as well as anyone who has to work with your code.

Bad Naming:

```javascript
const a = [1, 2, 3, 4, 5];
const a1 = [1, 4, 9, 16, 25];
```

Good naming:

```javascript
const numbers = [1, 2, 3, 4, 5];
const squares = [1, 4, 9, 16, 25];
```

This also applies to functions

Bad Naming:

```javascript
const numbers = [1, 2, 3, 4, 5];

function calc(n) {
  return n * n;
}

const squares = numbers.map(calc);
```

Good naming:

```javascript
const numbers = [1, 2, 3, 4, 5];

function squareNumber(number) {
  return number * number;
}

const squares = numbers.map(squareNumber);
```

Naming is tricky for every engineer. But that does not mean you don't do it. It means you think about what to name it **while** you are writing code, not after you have finished.

### 4. Best Practices: if else

Keeping your code as readable as possible will ensure that you don't run into issues later on. One way to ensure this with `if else if` is to always use `{}` with the statements.

Bad Usage:

```javascript
let sum = 10;

if (value % 2 == 0) sum += value;
else if (value == 0) sum += 1;
else sum += -1;
```

Good usage:

```javascript
let sum = 10;

if (value % 2 == 0) {
  sum += value;
} else if (value == 0) {
  sum += 1;
} else {
  sum += -1;
}
```

This ensures that there is never any confusion about which code runs and when.

It also ensures that there will be no chance of code not getting executed because it was placed on an adjacent line and is missed by the interpreter.



## 7. Higher Order Array Functions

Watch the videos and then practice

- [https://www.youtube.com/watch?v=zdp0zrpKzIE](https://www.youtube.com/watch?v=zdp0zrpKzIE)
- [https://www.youtube.com/watch?v=rRgD1yVwIvE](https://www.youtube.com/watch?v=rRgD1yVwIvE)

## 8. Best Practices: Higher Order Functions

When writing higher order functions, it is tempting to write all the functionality in one line of code. The problem with this approach is that it makes the code much harder to read and much harder to debug.

To make your code better, always use arrow functions with a `{}` block. That way, it becomes more readable and it makes things easier to work with for yourself and for other people who will be working with your code.

Bad Usage:
```javascript
const arr = [1, 2, 3, 4, 5];

const numbers = arr.filter(number => number > 5).map(number => number * number).reduce((acc, number) => acc + number);
```

Bad Usage:
```javascript
const arr = [1, 2, 3, 4, 5];

const numbers = arr.filter(number => number > 5)
.map(number => number * number)
.reduce((acc, number) => acc + number);
```

Good Usage:
```javascript
const arr = [1, 2, 3, 4, 5];

const numbers = arr.filter((number) => {
    return number > 5;
})
.map((number) => {
    return number * number;
})
.reduce((acc, number) => {
    acc = acc + number;

    return acc;
})
```

Here it becomes much more clear where all the functionality is happening and it has become much easier to work with. Making changes also becomes much easier since we would anyway have to have added the `{}` block.

This practice must also be followed when there is only 1 higher order function used. This will ensure that you do not fall into the bad practice of writing single line functions.

Bad Usage:
```javascript
const arr = [1, 2, 3, 4, 5];

const numbers = arr.filter(number => number > 5);
```

Good Usage:
```javascript
const arr = [1, 2, 3, 4, 5];

const numbers = arr.filter((number) => {
    return number > 5;
});
```