---
title: Sorting in JavaScript
date: 2023-03-06 15:35:29
tags: [JavaScript]
---
# Sort



* In JavaScript the sort() method sorts the elements of an array. Instead of returning a new array, the sort() method changes the position of the element within the array.

* The sort() method sorts the elements of an array in ascending order by default. It sorts the elements by converting them into strings and compares the strings by using [UTF-16](https://www.ibm.com/docs/en/i/7.2?topic=unicode-utf-16) code unit values to decide the order.

### String sorting

* Let's take an example to sort the array of strings: 

        let myArray = ['hello', 'this', 'is', 'madan', 'gopal'];

        console.log(myArray); // Output: [ 'hello', 'this', 'is', 'madan', 'gopal' ]
        myArray.sort();
        console.log(myArray); // Output: [ 'gopal', 'hello', 'is', 'madan', 'this' ]

* In the above code we can see that our array of a string is sorted according to alphabetical order.

* Let's take another example of an array which contains uppercase and lowercase.

        let fruits = [ 'Banana', 'apple', 'Date', 'carrot' ];

        console.log(fruits); // Output : [ 'Banana', 'apple', 'Date', 'carrot' ]
        myArray.sort();
        console.log(fruits); // Output : [ 'Banana', 'Date', 'apple', 'carrot' ]

* In the above code we can see that even if the letter D is greater than 'a' and 'c' it comes before in the array. It is because the sort() method is using each character's Unicode code point value to compare the values.

* To overcome this situation we need to pass a comparer function as an argument to the sort method. The comparer function will take two arguments first and second, it will return a value that will decide the sort order.


        function comparer(first, second){
            //
        }



Here's an example to sort the above problem in strings.

        let fruits =  [ 'Banana', 'apple', 'Date', 'carrot' ];

        function comparer(first, second){
            let item1 = first.toUpperCase();
            let item2 = second.toUpperCase();
            if(item1 > item2){
                return 1;
            } 
            if(item1 < item2){
                return -1;
            }
            return 0;
        }

        fruits.sort(comparer);

        console.log(fruits); // Output : [ 'apple', 'Banana', 'carrot', 'Date' ]

In the above code sort() method is accepting a callback function comparer and the callback function is taking two arguments first and second and returns a value by subtracting them. 
* If comparer returns a value which is less than zero then 'first' will come in the first place.
* If comparer returns a value which is greater than zero then 'second' will come in the first place.
* And if a comparer returns 0 then the sort method will make no changes in their position.


### Integers sorting
* Let's take an example for sorting an array of integers.

        let myArray = [ 3, 1, 10, 5, 8 ];
        myArray.sort();
        console.log(myArray); // Output: [ 1, 10, 3, 5, 8 ]

In the above code Even if 10 is the greatest number within the array it comes in second place after using the sort method. Same as the previous examples.

* To overcome this kind of situation again, we need to pass a function as an argument inside the sort() method. 

        let myArray = [ 3, 1, 10, 5, 8 ];
        
        function comparer(first, second){
            return first - second;
        }
        myArray.sort(comparer);
        console.log(myArray); //Output : [ 1, 3, 5, 8, 10 ]

* In the above code we are passing a callback function into the sort() method, callback function is taking two parameters 'first' and 'second'. Since it is an integer array we are simply subtracting the first and second numbers and returning the result. according to the returned result, the sort() method will decide its position in the array.

* We can also write the function within the sort() method It will also work the exact same way. like the example given below.

        let intArray = [3, 1, 10, 5, 8];

        myArray.sort((first, second) => {
                return first - second;
        })

        console.log(myArray);

### Floating point number sorting

* Let's take another example of floating point numbers.

        let floatArray = [1.23, 5.65, 10.54, 3.62, 7.58, 0.1];

        floatArray.sort();
        console.log(floatArray); // Output : [ 0.1, 1.23, 10.54, 3.62, 5.65, 7.58 ]

* In the above code the array doesn't seem sorted when calling the sort() method without passing any argument.
* To sort the array for the floating point we need to pass a callback function which will take two arguments.
* Let's take an example:

                let floatArray = [1.23, 5.65, 10.54, 3.62, 7.58, 0.1];

                function comparer(first, second){
                        return first - second;
                }

                floatArray.sort(comparer);
                console.log(floatArray); // Output : [ 0.1, 1.23, 3.62, 5.65, 7.58, 10.54 ]


### Objects Sorting

* In JavaScript, we can sort an array of objects based on the value of a specific property of each object. To do this, we can use the "sort" method and pass a comparer function that defines the sorting criteria.

* Let's say we have an array of objects which contains information about different people of different ages.


        let persons  = [
                {
                  id: 1,
                  Name: "Mike",
                  Age: 35
                },
                {
                  id: 2, 
                  Name: "John", 
                  Age: 26
                },
                {
                  id : 3, 
                  Name: "Michel", 
                  Age: 40
                },
                {
                  id: 4, 
                  Name: "Sara", 
                  Age: 19
                },
                {
                  id: 5, 
                  Name: "David",
                  Age: 24
                },
        ];


        persons.sort((first, second)=>{
                let person1 = first.Age;
                let person2 = second.Age;

                return person1 - person2;
        })

        console.log(persons);

* In the above code, objects were sorted based on their Age property. and we get the output as shown below:
#### Output: 
        
        [
        { id: 4, Name: 'Sara', Age: 19 },
        { id: 5, Name: 'David', Age: 24 },
        { id: 2, Name: 'John', Age: 26 },
        { id: 1, Name: 'Mike', Age: 35 },
        { id: 3, Name: 'Michel', Age: 40 }
        ]

### Object sorting based on multiple keys

* We can sort an array of objects based on the value of multiple properties of each object. To do this, we can use the '||' operator. 

* Let's take an example: 

        let students = [
                {
                  id: 1, 
                  Name: "Mike", 
                  Age: 35, 
                  Rollno: 543
                },
                {
                  id: 2, 
                  Name: "John", 
                  Age: 26, 
                  Rollno: 123
                },
                {
                  id : 3, 
                  Name: "Michel", 
                  Age: 35, 
                  Rollno: 475
                },
                {
                  id : 4, 
                  Name: "Sara", 
                  Age: 19, 
                  Rollno: 321
                },
                {
                  id: 5, 
                  Name: "David", 
                  Age: 24, 
                  Rollno: 001
                },
        ];

        students.sort((first, second)=>{
                return (first.Age - second.Age || first.Rollno - second.Rollno);
        })

        console.log(students);

* In the above code we are sorting multiple keys in an array of objects the first condition is to sort by 'Age' and the second is to sort by 'Rollno'. By using the '||' operator. 
* code will first sort the data by 'Age' and if multiple ages become the same then it will sort them by roll no.
#### Output:

          [
                { id: 4, Name: 'Sara', Age: 19, Rollno: 321 },
                { id: 5, Name: 'David', Age: 24, Rollno: 1 },
                { id: 2, Name: 'John', Age: 26, Rollno: 123 },
                { id: 3, Name: 'Michel', Age: 35, Rollno: 475 },
                { id: 1, Name: 'Mike', Age: 35, Rollno: 543 }
          ]
* We can see that 'Mike' and 'Michel' has the same age and they are sorted based on their 'Rollno'.


## Conclusion
* By Default the sort() method sorts the array in ascending order.
* We need to pass a callback function inside the sort() method to sort the elements of the array.
* The callback function will take two arguments and return the value after checking both values either it is greater than, less than or equal to zero. Based on the return values of the callback function sort() method decides its position.
* Sort() method does not make a copy of the array rather it will change the position of the elements within the array.
