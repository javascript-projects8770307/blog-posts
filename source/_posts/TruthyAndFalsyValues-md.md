---
title: Truthy And Falsy Values.md
date: 2023-02-22 14:59:53
tags: [JavaScript]
---
# Truthy Values
* In JavaScript Truthy values are those values in which there is some data (Apart from Falsy Values) and we can check them using the if condition.
* For example : 

		const value = 10;
		if(value){
			console.log('This is a Truthy Value');
		}
		// 'This is a Truthy Value'
		
* In the above code we are checking if there is a value inside the variable 'value'. And yes we are already holding '10' as a value inside the variable 'const value'. It will return true in a boolean context. which is why we can get inside the 'if' condition.

* What will happen if we declare a variable using 'let' but not assigning any value to the variable? Let's see.

		let value;
		if(value){
			console.log('This is a Truthy Value');
		}
		else{
			console.log('This is not a Truthy Value');
		}
		// This is not a Truthy Value'
		
* In the above code our if condition returns false in a boolean context. That's why we are going inside the else condition. We can see that we have declared a variable 'let value;' but not have assigned any value to the variable. So if a variable has no value assigned to it then it will be considered undefined in JavaScript. We can also check the type of a variable by using 'typeof'.

		let value;
		console.log(typeof value);
		//undefined
		
* In Javascript not only 'undefined' but there are a lot more which will return false in a boolean context. And here comes the Falsy Values.


# Falsy Values

* In JavaScript there are a few values which are considered 'Falsy values'. Those values are...
	* null
	* undefined
	* 0
	* false
	* ''
	* ""
	* NaN
* If a variable is holding any one of these values then it will be considered as 'Falsy Values'.
* We can also check them using the 'if' condition in our program. 
* For example: 

		let value = '';
		if(value){
			console.log('This is a Truthy Value');
		}
		else{
			console.log('This is a Falsy Value');
		}
		// 'This is a Falsy Value.
		
* In the above code the reason why we are getting 'This is a Falsy Value' is that Falsy values always return false in a Boolean context. So if a variable has been assigned to any one of the falsy values, will be considered as a Falsy Value.
* Apart from these values if a variable is holding anything else will be considered a Truthy Value.
* For example: 

		let value = 'Anything but not falsy';
		if(value){
			console.log('This is a Truthy Value');
		}
		else{
			console.log('This is a Falsy Value');
		}
		// 'This is a Truthy Value'
