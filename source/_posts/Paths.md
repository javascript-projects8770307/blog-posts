---
title: Paths in Node.js
date: 2023-03-15 15:18:57
tags: [JavaScript]
---


![Path Module](https://res.cloudinary.com/practicaldev/image/fetch/s---deNlEP2--/c_imagga_scale,f_auto,fl_progressive,h_900,q_auto,w_1600/https://dev-to-uploads.s3.amazonaws.com/i/jet0noqy55n2l7qrgoix.png)

# Path
The path module, which is built-in to Node.js, enables developers to interact with file paths in a way that is not platform-specific. This is especially beneficial when creating applications that involve file paths across different operating systems, as file paths may have various representations depending on the platform being used.

#### Using Path module.

The path is the core module of Node.js, so we can use it without installing.

        const path = require('path');
We can use the above syntax to use the path module in our code.



### How to use path.join()

The path.join() method in Node.js is used to concatenate two or more path segments into a single platform-independent path. The result path will normalized and it returns the normalized path as a string.

This method takes multiple arguments, each will represent a path segment, and joins them using the platform specific seperator character such as ("/" on Unix-based systems and "\\" on Windows).

The path is normalized to remove any redundant seperators and resolve any relative path segments such as ("." or "..").

Let's see an example: 

        const path = require('path');

        const directory = 'home/Desktop/Newfolder';
        const fileName = 'index.html';

        const fullPath = path.join(directory, fileName);

        console.log(fullPath);

Output

    home/Desktop/Newfolder/index.html

In the above code we are using the path module which Node.js has provided to interact with file paths easily.
We have created a variable "directory" which is a string. and another variable "filename" which is also a string.
Using path.join() method we are passing both variable as an argument and storing the value as "fullpath".
The path.join() methods returns a full path by joining the both the variable as string.


Let's take another example by using relative path segments.

        const path = require('path');

        const fullPath = path.join('home', 'Desktop', '..', 'Downloads/New\ Folder','index.html');

        console.log(fullPath)
Output

    home/Downloads/New Folder/index.html

In the above code path.join is taking multiple arguments as strings and we can notice there is a '..' which is passed as argument in the method, The '..' is a relative path and it refers to the parent directory. The path.join resolve this relative path segment and returns the path as a string.

## How to use path.resolve()

path.resolve() is a core method in Node.js which is used to resolve an absolute path from the given path string or path segments. The method  takes multiple path segments as a arguments and returns a string as a resolved absolute path.

If we use path.resolve() method without passing any arguments then it will show the current working directory as shown below

        const fs = require('fs');
        console.log(path.resolve());

Output

        /home/mg/Desktop/work/JavaScript_work

In the above code, we can see that the absolute path has been returned by the path.resolve() method.

The method resolves an absolute path by starting with the rightmost argument and then resolving each subsequent argument to the left until an absolute path is constructed.

Example 1: 

        const path = require('path');
        const fullPath = path.resolve("myFolder", "..", "index.cjs")
        console.log(fullPath);

Output:

        /home/mg/Desktop/work/JavaScript_work/index.cjs


Example 2:

        const path = require('path');
        const fullPath = path.resolve("myFolder/html", "htmlFiles", "index.html");
        console.log(fullPath);

Output:

        /home/mg/Desktop/work/JavaScript_work/myFolder/html/htmlFiles/index.html

In Both examples the path.resolve() methods return the absolute path that starts from root and goes all the way to the working directory.

Let's see another example:

        const path = require('path');

        const folder = "/Desktop/NewFolder";
        const file = "index.html";

        const fullPath = path.resolve(folder, file);

        console.log(fullPath);
Output:

        /Desktop/NewFolder/index.html

In the above code we are using the forward-slash at the starting of the folder name. Here the path.resolve method resolves the absolute path starting from the righmost until it construct the absolute path.

The path module is platform specific. And we use this module to manipulate the file path effectively.
