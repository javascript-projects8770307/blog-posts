---
title: Pass By Value And Pass By Reference
date: 2023-03-02 07:25:04
tags: [JavaScript]
---
## Pass By Value
 In Javascript when we use a primitive datatype variable (such as numbers, strings, or booleans) to pass as an argument inside any function. The function makes its copy of the variable. 
* Any changes made inside the function on that particular variable do no affect the original value of the variable. It will remain inside the scope of the function.
* Let's see with an example: 


        function square(number) {
            number = number * number;
            return number;
        }

        let myNumber  = 4;

        console.log(square(myNumber)); //It will print 16
        console.log(myNumber); // It will print 4

* In this example we can see that we have created a function of the name 'square' which is taking a number as an argument and performing some manipulation on the variable and returning the manipulated value.

* We have created a variable 'myNumber' and assigned the value 4 to it. And while calling the function we are passing the 'myNumber' variable as an argument.

* Even though the function has performed some manipulation on the variable which is passed, it is not changing the value of the original variable 'myNumber' which is created.

* Changes can be seen inside the function for its arguments. 

## Pass By Reference 
* When a function is called by passing the reference or address as an argument to a function then it is called Pass By Reference.

* In JavaScript when we call a function by passing any array or object as an argument. It is called Pass By Reference. That is because when we are passing an array or an objective function does not make the copy rather it will take the address of the memory where the array or object is stored.

* Performing any manipulation on an array or object in a function will also modify the original array or object.

* Let's see with an example:

        function callMe(myArray){
            myArray.push(5);
            return myArray;
        }

        let myArray = [1, 2, 3, 4];
        
        console.log(myArray); //Output: [1, 2, 3, 4]

        console.log(callMe(myArray)); // Output: [1, 2, 3, 4, 5]

        console.log(myArray); // Output: [1, 2, 3, 4, 5]

    
* In the above code example, we are creating an array which has 4 elements [1, 2, 3, 4]. After calling the function by passing that array as an argument to that function. The function is pushing an extra element '5'. By doing this in function scope it is also modifying the size of the array which is declared outside of the function. 

* When we try to check the array which we have created we can see it modified already.

* When we pass the array to the function it is not making a copy of the array instead it is taking the reference to the memory and performing the action on that.

* Let's see another example by passing an object...

        function callMe(myObject){

            myObject.lastName = 'Gopal';
            return myObject;
        }

        let myObject = {firstName : 'Madan'};
        console.log(myObject); // Output: {firstName : 'Madan'}

        console.log(callMe(myObject)); //Output : {firstName : 'Madan', lastName : 'Gopal'}

        console.log(myObject);//Output : {firstName : 'Madan', lastName : 'Gopal'}

* In the above code, we are creating an object 'myObject' which contains a key/value pair "firstName:'Madan'", when we are trying to check the object value it is showing the same result.
* After that we are calling the function 'callMe' by passing the object, the function is adding a new key/value pair of 'lastname : 'Gopal'.
* Our function is modifying the object which is passed by adding a new key/value pair. A modified object will not remain modified only inside the function but outside of the function also. Because it is acting on the reference of the object.