---
title: HOF
date: 2024-04-03 12:14:49
tags: [FullStack Path]
categories: [Higher Order Function]
---

### Higher Order Function

A higher order function is a function that takes one or more functions as arguments, or returns a function as its result. There are several different types of higher order functions like map and reduce.
[Watch this Youtube Tutorial](https://www.youtube.com/watch?v=zdp0zrpKzIE&pp=ygUiaGlnaGVyIG9yZGVyIGZ1bmN0aW9uIGFrc2hheSBzYWluaQ%3D%3D)